#!/bin/bash

cat <<END
+--------------------------------------+
|                                      |
|            Pure-Ftp 安装             |
|                                      |
+--------------------------------------+
END

# Check if user is root
if [ $(id -u) != "0" ]; then
    echo "Error: 请用 root 账户运行脚本"
    exit 1
fi
basepath=$(cd `dirname $0`; pwd)
cd $basepath
cur_dir=$(pwd)

##=====commonFun===
. include/func.sh
##=================

##====softVersion=====
PureFtp_Ver='pure-ftpd-1.0.46'
##====================
Echo_Blue "[+] Installing ${PureFtp_Ver}"

PureFtpUserName="pureftp"
adduser $PureFtpUserName -s /sbin/nologin -d /home/$PureFtpUserName

FtpUserUid=`id -G $PureFtpUserName`
FtpUserGid=`id -u $PureFtpUserName`


PureFtpInstallDir=$1
MysqlDir=$2
MysqlHost=$3
MysqlPort=$4
MysqlUser=$5
MysqlPwd=$6
MysqlDB=$PureFtpUserName



if [ "${MysqlDir}" = "" ]; then
	read -p "请输入 Mysql 目录: " MysqlDir
fi

if [ "${MysqlHost}" = "" ]; then
	read -p "请输入 Mysql 主机: " MysqlHost
fi
if [ "${MysqlPort}" = "" ]; then
	read -p "请输入 Mysql 端口: " MysqlPort
fi

if [ "${MysqlUser}" = "" ]; then
	read -p "请输入 Mysql 用户名: " MysqlUser
fi
if [ "${MysqlPwd}" = "" ]; then
	read -p "请输入 Mysql 密码: " MysqlPwd
fi


if [ ! -n "$MysqlDir" ] || [ ! -n "$MysqlHost" ]; then	
	cat <<END
>  Mysql 配置信息
>  MysqDir:  ${MysqlDir}
>  MysqlHost:${MysqlHost}
>  MysqlPort:${MysqlPort}
>  MysqlUser:${MysqlUser}
>  MysqlPwd: ${MysqlPwd}
>  MysqlDB:  ${MysqlDB}            

END
	Echo_Red "MySql 配置不完整,退出脚本!"
	exit 1
fi



PureFtpInitSql=`cat <<EOF
CREATE DATABASE IF NOT EXISTS ${MysqlDB};
use ${MysqlDB};
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS users;
CREATE TABLE users (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  User varchar(255) NOT NULL,
  Password varchar(255) NOT NULL,
  Uid int(11) NOT NULL,
  Gid int(11) NOT NULL,
  Dir varchar(255) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
INSERT INTO users (User, Password, Uid, Gid, Dir) VALUES ('${PureFtpUserName}', '123456', '${FtpUserUid}', '${FtpUserGid}', '/home/${PureFtpUserName}');
EOF`

MySqlRet=`${MysqlDir%*/}/bin/mysql -h${MysqlHost}  -P${MysqlPort} -u${MysqlUser} -p${MysqlPwd} -e"${PureFtpInitSql}"`

if [ $? != 0 ]; then
	echo $MySqlRet
	Echo_Red "MySql 执行失败,请确认 MySql 配置!"
	exit 1
fi

Echo_Yellow "成功创建库：${MysqlDB} 表：users"

if [ "${PureFtpInstallDir}" = "" ]; then
	PureFtpInstallDir="/usr/local"
	Echo_Yellow "设置PureFtp软件安装路径 ( 默认: ${PureFtpInstallDir})"
	read -p "请输入安装路径: " PureFtpInstallDir
	if [ "${PureFtpInstallDir}" = "" ]; then
		PureFtpInstallDir="/usr/local"
	fi

	if [ ! -d "${PureFtpInstallDir}" ]; then
		mkdir ${PureFtpInstallDir}
	fi

	if [ "$?" != 0 ];then
		Echo_Red "目录创建失败,检测安装目录是否有问题"
		exit 1
	fi
	
	
	DirConfirm="Y"
	Echo_Yellow "Pure-Ftp将安装到: [${PureFtpInstallDir%*/}/pure-ftp] 是否正确？"
	read -p "是否继续 [Y/n]: " DirConfirm

	case "${DirConfirm}" in
	[yY][eE][sS]|[yY])
		echo "软件将安装到 ${PureFtpInstallDir%*/}/pure-ftp"
	;;
	[nN][oO]|[nN])
		echo "取消操作"
		exit 0
	;;
	*)
		echo "软件将安装到 ${PureFtpInstallDir%*/}/pure-ftp"
	esac
fi


Tar_Cd ${PureFtp_Ver}.tar.gz ${PureFtp_Ver}

./configure --prefix=${PureFtpInstallDir%*/}/pure-ftp --with-language=english --with-everything --with-mysql=${MysqlDir%*/} --with-rfc2640
make install-strip




echo "Copy new pure ftp configure file..."
cp ${cur_dir}/etc/pure-ftp/welcome.msg ${PureFtpInstallDir%*/}/pure-ftp/etc/welcome.msg
cp ${cur_dir}/etc/pure-ftp/pure-ftpd.conf ${PureFtpInstallDir%*/}/pure-ftp/etc/pure-ftpd.conf
cp ${cur_dir}/etc/pure-ftp/pure-ftpd.conf ${PureFtpInstallDir%*/}/pure-ftp/etc/pure-ftpd.conf
cp ${cur_dir}/etc/pure-ftp/pureftpd-mysql.conf ${PureFtpInstallDir%*/}/pure-ftp/etc/pureftpd-mysql.conf



cd ${cur_dir}
echo "Modify pureftpd-mysql.conf......"
sed -i "s/#mysqlHost#/${MysqlHost}/g" ${PureFtpInstallDir%*/}/pure-ftp/etc/pureftpd-mysql.conf
sed -i "s/#mysqlPort#/${MysqlPort}/g" ${PureFtpInstallDir%*/}/pure-ftp/etc/pureftpd-mysql.conf
sed -i "s/#mysqlUser#/${MysqlUser}/g" ${PureFtpInstallDir%*/}/pure-ftp/etc/pureftpd-mysql.conf
sed -i "s/#mysqlPassword#/${MysqlPwd}/g" ${PureFtpInstallDir%*/}/pure-ftp/etc/pureftpd-mysql.conf
sed -i "s/#mysqlDB#/${MysqlDB}/g" ${PureFtpInstallDir%*/}/pure-ftp/etc/pureftpd-mysql.conf

sed -i "s:#welcomeMsg#:${PureFtpInstallDir%*/}/pure-ftp/etc/welcome.msg:g" ${PureFtpInstallDir%*/}/pure-ftp/etc/pure-ftpd.conf
sed -i "s:#mysqlConf#:${PureFtpInstallDir%*/}/pure-ftp/etc/pureftpd-mysql.conf:g" ${PureFtpInstallDir%*/}/pure-ftp/etc/pure-ftpd.conf



echo "Copy pure ftp init.d file..."
cp ${cur_dir}/etc/pure-ftp/pure-ftpd.init /etc/init.d/pure-ftpd

echo "Modify pure ftp init.d......"
sed -i "s:#SBIN#:${PureFtpInstallDir%*/}/pure-ftp/sbin/pure-ftpd:g" /etc/init.d/pure-ftpd
sed -i "s:#CONF#:${PureFtpInstallDir%*/}/pure-ftp/etc/pure-ftpd.conf:g" /etc/init.d/pure-ftpd
chmod +x /etc/init.d/pure-ftpd


Echo_Yellow "PureFtp安装成功。"

