#!/bin/bash

cat <<END
+--------------------------------------+
|                                      |
|      Centos7安装Apache httpd         |
|                                      |
+--------------------------------------+
END


# Check if user is root
if [ $(id -u) != "0" ]; then
    echo "Error: 请用 root 账户运行脚本"
    exit 1
fi

cur_dir=$(pwd)

##=============commonFun=============
. include/func.sh
##=========================

##=== 设置安装目录 ===
HttpdInstallDir="/usr/local/"
Echo_Yellow "设置 httpd 安装路径 ( 默认: ${HttpdInstallDir})"
read -p "请输入安装路径: " HttpdInstallDir
if [ "${HttpdInstallDir}" = "" ]; then
	HttpdInstallDir="/usr/local/"
fi

if [ ! -d "${HttpdInstallDir}" ]; then
	mkdir -p ${HttpdInstallDir}
fi

if [ "$?" != 0 ];then
	Echo_Red "目录创建失败,检测安装目录是否有问题"
	exit 1
fi

DirConfirm="Y"
HttpdInstallDir=${HttpdInstallDir%*/}/httpd
Echo_Yellow "请确认你的安装目录: [${HttpdInstallDir%*/}] 是否正确？"
read -p "是否继续 [Y/n]: " DirConfirm


case "${DirConfirm}" in
[yY][eE][sS]|[yY])
	echo "httpd 将安装到 ${HttpdInstallDir%*/}"
;;
[nN][oO]|[nN])
	echo "取消操作"
	exit 0
;;
*)
	echo "httpd 将安装到 ${HttpdInstallDir%*/}"
esac

##===



##=== 软件版本定义 ===
AprVer16="apr-1.6.5"
AprUtilVer16="apr-util-1.6.1"
HttpdVer24="httpd-2.4.37"


##==== arp安装  ====
Echo_Blue "[+] Installing ${AprVer16}"

#判断是否已经安装
if [ ! -d "/usr/local/apr/bin/" ];then
	
	Tar_Cd ${AprVer16}.tar.gz ${AprVer16}
	./configure --prefix=/usr/local/apr
	make
	make install
	Echo_Green "=== ${AprVer16} 安装完成 ==="

else
	Echo_Yellow " === 已经安装过apr，跳过 ==="
fi

sleep 1


##==== 

##==== arp-util安装  ====
Echo_Blue "[+] Installing ${AprUtilVer16}"

#判断是否已经安装apr-util
if [ ! -d "/usr/local/apr-util/bin/" ];then

	Tar_Cd ${AprUtilVer16}.tar.gz ${AprUtilVer16}
	./configure --prefix=/usr/local/apr-util --with-apr=/usr/local/apr/
	make
	make install
	Echo_Green "=== ${AprUtilVer16} 安装完成 ==="
	

else
	Echo_Yellow " === 已经安装过apr-util，跳过 ==="
fi

sleep 1


##=== httpd 安装 ===
Echo_Blue "[+] Installing ${HttpdVer24}"

Tar_Cd ${HttpdVer24}.tar.gz ${HttpdVer24}
./configure --prefix=${HttpdInstallDir} --with-apr=/usr/local/apr --with-apr-util=/usr/local/apr-util/
make
make install

# 调整 httpd.conf 配置
echo "Modify httpd.conf......"
cp ${cur_dir}/etc/httpd.conf ${HttpdInstallDir}/conf/httpd.conf
sed -i "s:/usr/local/httpd:${HttpdInstallDir}:g" ${HttpdInstallDir}/conf/httpd.conf

echo "Copy init.d httpd......"
cp ${cur_dir}/etc/init.d.httpd /etc/init.d/httpd
chmod +x /etc/init.d/httpd
sed -i "s:/usr/local/httpd:${HttpdInstallDir}:g" /etc/init.d/httpd



Echo_Green "=== ${HttpdVer24} 安装完成 ==="
##====





exit 0