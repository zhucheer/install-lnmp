#!/bin/bash

cat <<END
+--------------------------------------+
|                                      |
|            Centos7安装SVN            |
|                                      |
+--------------------------------------+
END


# Check if user is root
if [ $(id -u) != "0" ]; then
    echo "Error: 请用 root 账户运行脚本"
    exit 1
fi
cur_dir=$(pwd)

##========commonFun=====
. include/func.sh
##=====

EvnHttpdStatus="Y"
Echo_Yellow "安装前请先确认已经安装好 httpd "
read -p "确认已经安装好? [Y/n]: " EvnHttpdStatus
case "${EvnHttpdStatus}" in
[yY][eE][sS]|[yY])
	echo "continue..."
;;
[nN][oO]|[nN])
	echo "取消操作"
	exit 0
;;
*)
	echo "continue..."
esac


##=== 设置安装目录 ===
SvnInstallDir="/usr/local/subversion"
Echo_Yellow "设置 snv 安装路径 ( 默认: ${SvnInstallDir})"
read -p "请输入安装路径: " SvnInstallDir
if [ "${SvnInstallDir}" = "" ]; then
	SvnInstallDir="/usr/local/subversion"
fi

if [ ! -d "${SvnInstallDir}" ]; then
	mkdir -p ${SvnInstallDir}
fi

if [ "$?" != 0 ];then
	Echo_Red "目录创建失败,检测安装目录是否有问题"
	exit 1
fi

DirConfirm="Y"
Echo_Yellow "请确认你的安装目录: [${SvnInstallDir%*/}] 是否正确？"
read -p "是否继续 [Y/n]: " DirConfirm

case "${DirConfirm}" in
[yY][eE][sS]|[yY])
	echo "httpd 将安装到 ${SvnInstallDir%*/}"
;;
[nN][oO]|[nN])
	echo "取消操作"
	exit 0
;;
*)
	echo "httpd 将安装到 ${SvnInstallDir%*/}"
esac

##===


##=== 设置安装目录 ===
ApxsPath="/usr/local/httpd/bin/apxs"
Echo_Yellow "依赖httpd中的apxs, 设置 httpd-apxs 路径 ( 默认: ${ApxsPath})"
read -p "请输入 apxs 路径: " ApxsPath
if [ "${ApxsPath}" = "" ]; then
	ApxsPath="/usr/local/httpd/bin/apxs"
fi

if [ ! -f "${ApxsPath}" ]; then
	Echo_Red "路径错误，未找到apxs"
	exit 0
fi

##===




##=== 软件版本定义 ===
SvnVer="subversion-1.11.0"
SqliteZip="sqlite-amalgamation-3081101"

##== 安装 svn
Tar_Cd ${SvnVer}.tar.gz ${SvnVer}
Unzip_Src ${SqliteZip}.zip ${SqliteZip}
mv ${SqliteZip} ./${SvnVer}/sqlite-amalgamation

cd ${cur_dir}/src/${SvnVer}

./configure --with-apxs=${ApxsPath} --with-apr=/usr/local/apr/ --with-apr-util=/usr/local/apr-util/ --prefix=${SvnInstallDir} --with-lz4=internal --with-utf8proc=internal

make && make install


Echo_Green "=== ${SvnVer} 安装完成 ==="
##====

