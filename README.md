# 自动化 LNMP 环境部署工具
## 包含环境部署和自动化运维脚本

#### 项目介绍
编译安装 nginx，mysql，php 费时费力，各种依赖，装个环境可能需要半天时间；
此工具将自动化安装 php 相关环境，附带各种自动化运维工具，运维小工具；
该脚本包含 Tengine2.2，Mysql5.5，PHP5.6，PHP7.0，PHP7.2，PureFtp，PHPMyAdmin



#### 安装教程
- src 目录为相关软件包，体积太大，请大家从百度云下载好后覆盖到根目录
- 链接：https://pan.baidu.com/s/1KAhAlxDPDbOzWDofST8baA 
- 提取码：wb28 
- 目前只支持与Red Hat系列Linux （Red Hat，Centos等）
- 暂不支持Ubuntu，Debian系列


#### 使用说明

- 执行 install.sh 脚本，完成提示操作即可开始安装 LNMP 环境；
```
chomd +x ./install.sh
./install.sh
```

- 执行 cenos7useiptables.sh ，将 Centos7 下自动切换成 iptables 防火墙，并自动设置成禁止入口流量模式，只开放 80，443，3306，6379端口;

```
chomd +x ./cenos7useiptables.sh
./cenos7useiptables.sh
```


- 执行 httpd.sh ，将自动安装 Apache httpd 服务，相关 php-fpm 默认走上面 php5.6 相关配置路径;

```
chomd +x ./httpd.sh
./httpd.sh
```

- 执行 subversion.sh ，将自动安装 Svn 服务;

```
chomd +x ./subversion.sh
./subversion.sh
```