#!/bin/bash

cat <<END
+----------------------------------+
|                                  |
|     欢迎使用自动LNMP安装工具     |
|           作者：小居             |
|   默认安装：                     |
|   Tengine-2.2.2；                |
|   Mysql-5.5.42；                 |
|   PHP-5.6.38；                   |
|   PureFtp-1.0.46                 |
|   PHPMyAdmin-4.8.5               |
|                                  |
+----------------------------------+
END


# Check if user is root
if [ $(id -u) != "0" ]; then
    echo "Error: 请用 root 账户运行脚本"
    exit 1
fi
basepath=$(cd `dirname $0`; pwd)
cd $basepath

cur_dir=$(pwd)


. include/tengine.sh
. include/mysql.sh
. include/php.sh
. include/phpMyAdmin.sh

##=============commonFun=============
. include/func.sh
##=========================

##======将脚本都添加执行权限====
chmod +x ${cur_dir}/pure-ftp.sh
chmod +x ${cur_dir}/cenos7useiptables.sh
chmod +x ${cur_dir}/subversion.sh
chmod +x ${cur_dir}/php.sh
##=======

##============softVersion=============
Libiconv_Ver='libiconv-1.14'
LibMcrypt_Ver='libmcrypt-2.5.8'
LibZip_Ver='libzip-1.2.0'
Mcypt_Ver='mcrypt-2.6.8'
Mash_Ver='mhash-0.9.9.9'
Freetype_Ver='freetype-2.4.12'
Mysql_Ver='mysql-5.5.42'
Pcre_Ver='pcre-8.36'
Tengine_Ver='tengine-2.2.2'
Php_Ver56='php-5.6.38'
Php_Ver70='php-7.0.32'
Php_Ver72='php-7.2.10'
##====================

##=================init===================

#删除已经安装的服务
CentOS_RemoveAMP()
{
    Echo_Blue "[-] Yum remove packages..."
    rpm -qa|grep httpd
    rpm -e httpd httpd-tools
    rpm -qa|grep mysql
    rpm -e mysql mysql-libs
    rpm -qa|grep php
    rpm -e php-mysql php-cli php-gd php-common php

    yum -y remove httpd*
    yum -y remove mysql-server mysql mysql-libs
    yum -y remove php*
    yum clean all
}

#设置时区，更新时间
CentOS_InstallNTP()
{
    Echo_Blue "[+] Installing ntp..."
    yum install -y ntp
    ntpdate -u pool.ntp.org
    date
}

#初始依赖
CentOS_Dependent()
{
    cp /etc/yum.conf /etc/yum.conf.lnmp
    sed -i 's:exclude=.*:exclude=:g' /etc/yum.conf

    Echo_Blue "[+] Yum installing dependent packages..."
    for packages in make cmake gcc gcc-c++ gcc-g77 automake pcre pcre-devel flex bison file libtool libtool-libs libaio freetype-devel autoconf kernel-devel patch wget libjpeg libjpeg-devel libpng libpng-devel libpng10 libpng10-devel gd gd-devel libxml2 libxml2-devel zlib zlib-devel glib2 glib2-devel unzip tar bzip2 bzip2-devel libevent libevent-devel ncurses ncurses-devel curl curl-devel e2fsprogs e2fsprogs-devel krb5 krb5-devel libidn libidn-devel openssl openssl-devel vim-minimal gettext gettext-devel ncurses-devel gmp-devel pspell-devel unzip libcap diffutils net-tools libc-client-devel psmisc libXpm-devel git-core c-ares-devel libicu-devel libxslt libxslt-devel sendmail;
    do yum -y install $packages; done

    mv -f /etc/yum.conf.lnmp /etc/yum.conf
}

#关闭selinux
Disable_Selinux()
{
    if [ -s /etc/selinux/config ]; then
        sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
    fi
}


Install_Libiconv()
{
    Echo_Blue "[+] Installing ${Libiconv_Ver}"
    Tar_Cd ${Libiconv_Ver}.tar.gz ${Libiconv_Ver}
    patch -p0 < ${cur_dir}/src/patch/libiconv-glibc-2.16.patch
    ./configure --enable-static
    make && make install
}

Install_Libmcrypt()
{
    Echo_Blue "[+] Installing ${LibMcrypt_Ver}"
    Tar_Cd ${LibMcrypt_Ver}.tar.gz ${LibMcrypt_Ver}
    ./configure
    make && make install
    /sbin/ldconfig
    cd libltdl/
    ./configure --enable-ltdl-install
    make && make install
    ln -s /usr/local/lib/libmcrypt.la /usr/lib/libmcrypt.la
    ln -s /usr/local/lib/libmcrypt.so /usr/lib/libmcrypt.so
    ln -s /usr/local/lib/libmcrypt.so.4 /usr/lib/libmcrypt.so.4
    ln -s /usr/local/lib/libmcrypt.so.4.4.8 /usr/lib/libmcrypt.so.4.4.8
    ldconfig
}

Install_LibZip()
{
	Echo_Blue "[+] Installing ${LibZip_Ver}"
	Tar_Cd ${LibZip_Ver}.tar.gz ${LibZip_Ver}
	./configure
    make && make install
}

Install_Mcrypt()
{
    Echo_Blue "[+] Installing ${Mcypt_Ver}"
    Tar_Cd ${Mcypt_Ver}.tar.gz ${Mcypt_Ver}
    ./configure
    make && make install
}

Install_Mhash()
{
    Echo_Blue "[+] Installing ${Mash_Ver}"
    Tar_Cd ${Mash_Ver}.tar.gz ${Mash_Ver}
    ./configure
    make && make install
    ln -s /usr/local/lib/libmhash.a /usr/lib/libmhash.a
    ln -s /usr/local/lib/libmhash.la /usr/lib/libmhash.la
    ln -s /usr/local/lib/libmhash.so /usr/lib/libmhash.so
    ln -s /usr/local/lib/libmhash.so.2 /usr/lib/libmhash.so.2
    ln -s /usr/local/lib/libmhash.so.2.0.1 /usr/lib/libmhash.so.2.0.1
    ldconfig
}

Install_Freetype()
{
    Echo_Blue "[+] Installing ${Freetype_Ver}"
    Tar_Cd ${Freetype_Ver}.tar.gz ${Freetype_Ver}
    ./configure --prefix=/usr/local/freetype
    make && make install

    cat > /etc/ld.so.conf.d/freetype.conf<<EOF
/usr/local/freetype/lib
EOF
    ldconfig
    ln -sf /usr/local/freetype/include/freetype2 /usr/local/include
    ln -sf /usr/local/freetype/include/ft2build.h /usr/local/include
}

Install_Pcre()
{
    Cur_Pcre_Ver=`pcre-config --version`
    if echo "${Cur_Pcre_Ver}" | grep -vEqi '^8.';then
        Echo_Blue "[+] Installing ${Pcre_Ver}"
        Tar_Cd ${Pcre_Ver}.tar.gz ${Pcre_Ver}
        ./configure
        make && make install
    fi
}

CheckSysMem()
{
	sysmem=`free | grep "Mem:" |awk '{print $2}'`

	if [ $sysmem -lt 1048567 ]; then
		Echo_Red "系统内存不足1G, 无法安装php7+"
		exit 0
	fi
}
##================




##=======start install==============
Press_Install
Get_Dist_Name


if [ "${DISTRO}" = "unknow" ]; then
    Echo_Red "暂时仅支持Red Hat系Linux"
    exit 1
fi

 
#step 1.
LnmpInstallDir="/usr/local/"
Echo_Yellow "设置Lnmp软件安装路径 ( 默认: ${LnmpInstallDir})"
read -p "请输入安装路径: " LnmpInstallDir
if [ "${LnmpInstallDir}" = "" ]; then
	LnmpInstallDir="/usr/local/"
fi

if [ ! -d "${LnmpInstallDir}" ]; then
mkdir ${LnmpInstallDir}
fi

if [ "$?" != 0 ];then
	Echo_Red "目录创建失败,检测安装目录是否有问题"
	exit 1
fi

DirConfirm="Y"
Echo_Yellow "请确认你的安装目录: [${LnmpInstallDir%*/}] 是否正确？"
read -p "是否继续 [Y/n]: " DirConfirm


case "${DirConfirm}" in
[yY][eE][sS]|[yY])
	echo "软件将安装到 ${LnmpInstallDir%*/}"
;;
[nN][oO]|[nN])
	echo "取消操作"
	exit 0
;;
*)
	echo "软件将安装到 ${LnmpInstallDir%*/}"
esac



#设置Mysql初始密码?
echo "==========================="
MysqlRootPWD="123456"
Echo_Yellow "设置 MySQL root账户密码( 默认密码: 123456)"
read -p "请输入一个密码: " MysqlRootPWD
if [ "${MysqlRootPWD}" = "" ]; then
	MysqlRootPWD="123456"
fi
echo "MySQL root 密码: ${MysqlRootPWD}"


#选择引擎支持?
echo "==========================="
InstallInnodb="y"
Echo_Yellow "是否开启 InnoDB 存储引擎?"
read -p "默认开启,输入 [Y/n]: " InstallInnodb

case "${InstallInnodb}" in
[yY][eE][sS]|[yY])
	echo "开启了 InnoDB 存储引擎"
;;
[nN][oO]|[nN])
	echo "关闭了 InnoDB 存储引擎!"
;;
*)
	echo "默认配置,开启了 InnoDB 存储引擎"
	InstallInnodb="y"
esac



#选择php版本?
echo "==========================="
PHPSelect="1"
Echo_Yellow "选择追加安装php版本套餐."
echo "1: 不追加默认只有 PHP 5.6.38 (Default)"
echo "2: 追加安装 PHP 7.0.32"
echo "3: 追加安装 PHP 7.2.10"
echo "4: 以上都追加安装"

read -p "Enter your choice (1, 2, 3 or 4): " PHPSelect

case "${PHPSelect}" in
1)
	echo "选择了不追加"
;;
2)
	CheckSysMem
	echo "选择了 PHP 7.0.32"
;;
3)
	CheckSysMem
	echo "选择了 PHP 7.2.10"
;;
4)
	CheckSysMem
	echo "选择了 全部安装"
;;
*)
	echo "默认选择,PHP 5.6.38 "
	PHPSelect=1
esac


#step 2. 开始安装依赖
CentOS_InstallNTP
CentOS_Dependent

Disable_Selinux
Install_Libiconv
Install_Libmcrypt
Install_Mhash
Install_Mcrypt
Install_Freetype
Install_LibZip

cat>>/etc/ld.so.conf<<EOF
/usr/local/lib64
/usr/local/lib
/usr/lib
/usr/lib64
EOF
ldconfig -v

#step 3. 安装Mysql
Install_MySQL_55

#step 4.安装Tengine
Install_Tengine


#step 5. 安装PHP
Install_PHP_56


if [ "${PHPSelect}" = 2 ]; then
    Install_PHP_70
fi

if [ "${PHPSelect}" = 3 ]; then
    Install_PHP_72
fi

if [ "${PHPSelect}" = 4 ]; then
	Install_PHP_70
    Install_PHP_72
fi


Creat_PHP_Tools

# 安装pure-ftp
/etc/init.d/mysql start
cd ${cur_dir}
echo "${cur_dir}/pure-ftp.sh ${LnmpInstallDir%*/} ${LnmpInstallDir%*/}/mysql55 127.0.0.1 3306 root ${MysqlRootPWD}"
echo `${cur_dir}/pure-ftp.sh ${LnmpInstallDir%*/} ${LnmpInstallDir%*/}/mysql55 127.0.0.1 3306 root ${MysqlRootPWD}`


# 安装phpMyAdmin
Install_PHPMyAdmin


echo -e "\n\n\n\n"
Echo_Green "=====安装完成========"

Echo_Blue "启动Mysql"
/etc/init.d/mysql start
Echo_Blue "启动PHP5.6"
/etc/init.d/php-fpm56 start
Echo_Blue "启动Tengine"
/etc/init.d/tengine start

Echo_Blue "启动PureFtp"
/etc/init.d/pure-ftpd start

ipaddr='127.0.0.1'
ipaddr=$(ip addr | awk '/^[0-9]+: / {}; /inet.*global/ {print gensub(/(.*)\/(.*)/, "\\1", "g", $2)}')
echo "==========================="
Echo_Yellow "请尝试访问 http://${ipaddr} 开启你的LNMP之旅。"
Echo_Yellow "如无法访问,请尝试关闭防火墙或执行 ./cenos7useiptables.sh 脚本。"




