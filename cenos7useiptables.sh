#!/bin/bash

cat <<END
+--------------------------------------+
|                                      |
|   Centos7使用iptables停用firewall    |
|                                      |
+--------------------------------------+
END

#iptables基本用法
#查看
#iptables -L -n --line-number

#删除
#iptables -D INPUT 2

#添加拒绝端口
#iptables -A INPUT -p tcp --dport 8080 -j DROP
#iptables -A OUTPUT -p tcp --sport 80 -j DROP


echo "静态防火墙规则配置文件是 /etc/sysconfig/iptables 以及 /etc/sysconfig/ip6tables "

##=====commonFun===
. include/func.sh
##=================

# Check if user is root
if [ $(id -u) != "0" ]; then
    echo "Error: 请用 root 账户运行脚本"
    exit 1
fi

cur_dir=$(pwd)

DirConfirm="Y"
Echo_Yellow "执行后将会清空当前iptables所有规则，确认继续？"
read -p "是否继续 [Y/n]: " DirConfirm

case "${DirConfirm}" in
[yY][eE][sS]|[yY])
	echo "清空当前iptables所有规则"
;;
[nN][oO]|[nN])
	echo "取消操作"
	exit 0
;;
*)
	echo "清空当前iptables所有规则"
esac


yum -y install iptables-services 
systemctl mask firewalld.service 
systemctl enable iptables.service 
systemctl enable ip6tables.service

systemctl stop firewalld.service 
systemctl start iptables.service 
systemctl start ip6tables.service

#清空当前规则
iptables -F

#首先对SSH进行ACCEPT配置，以免直接无法连接的情况发生:
iptables -I INPUT -p tcp --dport 22 -j ACCEPT
iptables -I OUTPUT -p tcp --sport 22 -j ACCEPT   

#放行80,443,3306,6379端口
iptables -I INPUT -p tcp --dport 80 -j ACCEPT 
iptables -I INPUT -p tcp --dport 443 -j ACCEPT 
iptables -I INPUT -p tcp --dport 3306 -j ACCEPT 
iptables -I INPUT -p tcp --dport 6379 -j ACCEPT

#放行pure-ftp服务
iptables -I INPUT -p tcp --dport 30000:50000 -j ACCEPT
iptables -I INPUT -p tcp --dport 21 -m state --state NEW,ESTABLISHED -j ACCEPT

# 放行icmp流量
iptables -I INPUT -p icmp -m icmp --icmp-type 8 -j ACCEPT


#入口流量全部拒绝,出口不做控制
iptables -A INPUT -p all -j REJECT --reject-with icmp-host-prohibited
iptables -P OUTPUT ACCEPT

service iptables save
Echo_Yellow "更新完成，iptables规则如下："

IPTABLESLIST=`iptables -L -n`

echo "$IPTABLESLIST"

