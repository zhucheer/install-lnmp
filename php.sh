#!/bin/bash

cat <<END
+--------------------------------------+
|                                      |
|            单独安装PHP环境           |
|         需先安装好php相关依赖        |
|                                      |
+--------------------------------------+
END

# Check if user is root
if [ $(id -u) != "0" ]; then
    echo "Error: 请用 root 账户运行脚本"
    exit 1
fi
basepath=$(cd `dirname $0`; pwd)
cd $basepath

cur_dir=$(pwd)
##=============commonFun===
. include/func.sh
. include/php.sh
##=========================


##====softVersion=====
Php_Ver56='php-5.6.38'
Php_Ver70='php-7.0.32'
Php_Ver72='php-7.2.10'
##====================

#选择php版本?
echo "==========================="
PHPSelect=1
PHPBaseDir="php56"
Echo_Yellow "选择追加安装php版本套餐."
echo "1: PHP 5.6.38 (Default)"
echo "2: PHP 7.0.32"
echo "3: PHP 7.2.10"

read -p "Enter your choice (1, 2 or 3): " PHPSelect

case "${PHPSelect}" in
1)
	echo "选择了 5.6.38"
;;
2)
	PHPBaseDir="php70"
	echo "选择了 PHP 7.0.32"
;;
3)
	PHPBaseDir="php72"
	echo "选择了 PHP 7.2.10"
;;
*)
	echo "默认选择,PHP 5.6.38"
	PHPSelect=1
esac

echo $PHPBaseDir

##=== 设置安装目录 ===
PhpInstallDir="/usr/local/"
Echo_Yellow "设置Lnmp软件安装路径 ( 默认: ${PhpInstallDir})"
read -p "请输入安装路径: " PhpInstallDir
if [ "${PhpInstallDir}" = "" ]; then
	PhpInstallDir="/usr/local/"
fi

if [ ! -d "${PhpInstallDir}" ]; then
	mkdir ${PhpInstallDir}
fi

if [ "$?" != 0 ];then
	Echo_Red "目录创建失败,检测安装目录是否有问题"
	exit 1
fi

echo "${PhpInstallDir%*/}/${PHPBaseDir}"
if [ -d "${PhpInstallDir%*/}/${PHPBaseDir}" ];then
	Echo_Red "${PhpInstallDir%*/}/${PHPBaseDir} 目录已经存在，请先删除此目录！"
	exit 1
fi

DirConfirm="Y"
Echo_Yellow "请确认你的安装目录: [${PhpInstallDir%*/}] 是否正确？"
read -p "是否继续 [Y/n]: " DirConfirm


case "${DirConfirm}" in
[yY][eE][sS]|[yY])
	echo "软件将安装到 ${PhpInstallDir%*/}/${PHPBaseDir}"
;;
[nN][oO]|[nN])
	echo "取消操作"
	exit 0
;;
*)
	echo "软件将安装到 ${PhpInstallDir%*/}/${PHPBaseDir}"
esac
LnmpInstallDir=${PhpInstallDir%*/}
##===



##=== start install
Press_Install
Get_Dist_Name

if [ "${DISTRO}" = "unknow" ]; then
    Echo_Red "暂时仅支持Red Hat系Linux"
    exit 1
fi
##===


case "${PHPSelect}" in
1)
	Install_PHP_56
;;
2)
	Install_PHP_70
;;
3)
	Install_PHP_72
;;
*)
	Install_PHP_56
esac

Creat_PHP_Tools

Echo_Green "=====安装完成========"
Echo_Green "安装目录：${PhpInstallDir%*/}/${PHPBaseDir}"

