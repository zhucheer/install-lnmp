#!/bin/bash

Install_PHPMyAdmin()
{
	echo ${cur_dir}
	PhpMyAdmin_Ver48='phpMyAdmin-4.8.5-all-languages'
	Echo_Blue "[+] Installing ${PhpMyAdmin_Ver48}"
    Tar_Cd ${PhpMyAdmin_Ver48}.tar.gz ${PhpMyAdmin_Ver48}

	mv ${cur_dir}/src/${PhpMyAdmin_Ver48} /home/wwwroot/default/phpMyAdmin
	
	mv /home/wwwroot/default/phpMyAdmin/config.sample.inc.php /home/wwwroot/default/phpMyAdmin/config.inc.php
	
	sed -i "s:cfg\['blowfish_secret'\] = '':cfg['blowfish_secret'] = 'git.oschina.net/zhucheer/install-lnmp':g" /home/wwwroot/default/phpMyAdmin/config.inc.php
	echo "\$cfg['TempDir']='/tmp/';" >> /home/wwwroot/default/phpMyAdmin/config.inc.php
	
}