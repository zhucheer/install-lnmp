#!/bin/bash

Install_Composer()
{
    curl -sS --connect-timeout 30 -m 60 https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
    if [ $? -eq 0 ]; then
        echo "Composer install successfully."
    else
        echo "Composer install error."
    fi
}

Install_PHP_56()
{
    Echo_Blue "[+] Installing ${Php_Ver56}"
    Tar_Cd ${Php_Ver56}.tar.gz ${Php_Ver56}
    
	./configure --prefix=${LnmpInstallDir%*/}/php56 --with-config-file-path=${LnmpInstallDir%*/}/php56/etc --enable-fpm --with-fpm-user=www --with-fpm-group=www --with-mysql=mysqlnd --with-mysqli=mysqlnd --with-pdo-mysql=mysqlnd --with-iconv-dir --with-freetype-dir=/usr/local/freetype --with-jpeg-dir --with-png-dir --with-zlib --with-libxml-dir=/usr --enable-xml --enable-bcmath --enable-shmop --enable-sysvsem --enable-inline-optimization --with-curl --enable-mbregex --enable-mbstring --enable-ftp --with-gd --with-openssl --with-mhash --enable-pcntl --enable-sockets --with-xmlrpc --enable-zip --enable-soap --with-gettext
    

    make ZEND_EXTRA_LIBS='-liconv'
    make install
	
    echo "Copy new php configure file..."
    mkdir -p ${LnmpInstallDir%*/}/php56/etc
    cp php.ini-development ${LnmpInstallDir%*/}/php56/etc/php.ini

    cd ${cur_dir}
    # php extensions
    echo "Modify php.ini......"
    sed -i 's/post_max_size = 8M/post_max_size = 32M/g' ${LnmpInstallDir%*/}/php56/etc/php.ini
    sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 32M/g' ${LnmpInstallDir%*/}/php56/etc/php.ini
    sed -i 's/;date.timezone =/date.timezone = Asia\/Shanghai/g' ${LnmpInstallDir%*/}/php56/etc/php.ini
    sed -i 's/short_open_tag = Off/short_open_tag = On/g' ${LnmpInstallDir%*/}/php56/etc/php.ini
    sed -i 's/; cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g' ${LnmpInstallDir%*/}/php56/etc/php.ini
    sed -i 's/; cgi.fix_pathinfo=0/cgi.fix_pathinfo=0/g' ${LnmpInstallDir%*/}/php56/etc/php.ini
    sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g' ${LnmpInstallDir%*/}/php56/etc/php.ini
    sed -i 's/max_execution_time = 30/max_execution_time = 300/g' ${LnmpInstallDir%*/}/php56/etc/php.ini

    echo "Creating new php-fpm configure file..."
    cat > ${LnmpInstallDir%*/}/php56/etc/php-fpm.conf<<EOF
[global]
pid = run/php-fpm.pid
error_log = log/php-fpm.log
log_level = notice

[www]
listen = /tmp/php-fpm56.sock
listen.backlog = -1
listen.allowed_clients = 127.0.0.1
listen.owner = www
listen.group = www
listen.mode = 0666
user = www
group = www
pm = dynamic
pm.max_children = 10
pm.start_servers = 2
pm.min_spare_servers = 1
pm.max_spare_servers = 6
request_terminate_timeout = 100
request_slowlog_timeout = 0
slowlog = log/slow.log
EOF

    echo "Copy php-fpm init.d file..."
    cp ${cur_dir}/src/${Php_Ver56}/sapi/fpm/init.d.php-fpm /etc/init.d/php-fpm56
    chmod +x /etc/init.d/php-fpm56
}

Install_PHP_70()
{
	Echo_Blue "[+] Installing ${Php_Ver70}"
	Tar_Cd ${Php_Ver70}.tar.gz ${Php_Ver70}
	
	./configure --prefix=${LnmpInstallDir%*/}/php70 --with-config-file-path=${LnmpInstallDir%*/}/php70/etc --enable-fpm --with-fpm-user=www --with-fpm-group=www --enable-mysqlnd --with-mysqli=mysqlnd --with-pdo-mysql=mysqlnd --with-iconv-dir --with-freetype-dir=/usr/local/freetype --with-jpeg-dir --with-png-dir --with-zlib --with-libxml-dir=/usr --enable-xml --disable-rpath --enable-bcmath --enable-shmop --enable-sysvsem --enable-inline-optimization --with-curl --enable-mbregex --enable-mbstring --enable-intl --enable-pcntl --with-mcrypt --enable-ftp --with-gd --enable-gd-native-ttf --with-openssl --with-mhash --enable-pcntl --enable-sockets --with-xmlrpc --enable-zip --enable-soap --with-gettext --enable-opcache --with-xsl
	
	make ZEND_EXTRA_LIBS='-liconv'
    make install
	
	echo "Copy new php configure file..."
    cp php.ini-development ${LnmpInstallDir%*/}/php70/etc/php.ini
	
	cd ${cur_dir}
    # php extensions
    echo "Modify php.ini......"
    sed -i 's/post_max_size =.*/post_max_size = 32M/g' ${LnmpInstallDir%*/}/php70/etc/php.ini
    sed -i 's/upload_max_filesize =.*/upload_max_filesize = 32/g' ${LnmpInstallDir%*/}/php70/etc/php.ini
    sed -i 's/;date.timezone =.*/date.timezone = Asia\/Shanghai/g' ${LnmpInstallDir%*/}/php70/etc/php.ini
    sed -i 's/short_open_tag =.*/short_open_tag = On/g' ${LnmpInstallDir%*/}/php70/etc/php.ini
    sed -i 's/;cgi.fix_pathinfo=.*/cgi.fix_pathinfo=0/g' ${LnmpInstallDir%*/}/php70/etc/php.ini
    sed -i 's/max_execution_time =.*/max_execution_time = 300/g' ${LnmpInstallDir%*/}/php70/etc/php.ini
	
	
	echo "Creating new php-fpm configure file..."
    cat > ${LnmpInstallDir%*/}/php70/etc/php-fpm.conf<<EOF
[global]
pid = run/php-fpm.pid
error_log = log/php-fpm.log
log_level = notice
include=${LnmpInstallDir%*/}/php70/etc/php-fpm.d/*.conf	
EOF

cat > ${LnmpInstallDir%*/}/php70/etc/php-fpm.d/www.conf<<EOF
[www]
listen = /tmp/php-fpm70.sock
listen.backlog = -1
listen.allowed_clients = 127.0.0.1
listen.owner = www
listen.group = www
listen.mode = 0666
user = www
group = www
pm = dynamic
pm.max_children = 10
pm.start_servers = 2
pm.min_spare_servers = 1
pm.max_spare_servers = 6
request_terminate_timeout = 100
request_slowlog_timeout = 0
slowlog = log/slow.log
EOF
	
	echo "Copy php-fpm init.d file..."
    cp ${cur_dir}/src/${Php_Ver70}/sapi/fpm/init.d.php-fpm /etc/init.d/php-fpm70
    chmod +x /etc/init.d/php-fpm70
}

Install_PHP_72()
{
	Echo_Blue "[+] Installing ${Php_Ver72}"
	Tar_Cd ${Php_Ver72}.tar.gz ${Php_Ver72}
	
	./configure --prefix=${LnmpInstallDir%*/}/php72 --with-config-file-path=${LnmpInstallDir%*/}/php72/etc --enable-fpm --with-fpm-user=www --with-fpm-group=www --enable-mysqlnd --with-mysqli=mysqlnd --with-pdo-mysql=mysqlnd --with-iconv-dir --with-freetype-dir=/usr/local/freetype --with-jpeg-dir --with-png-dir --with-zlib --with-libxml-dir=/usr --enable-xml --disable-rpath --enable-bcmath --enable-shmop --enable-sysvsem --enable-inline-optimization --with-curl --enable-mbregex --enable-mbstring --enable-intl --enable-pcntl --with-mcrypt --enable-ftp --with-gd --with-openssl --with-mhash --enable-pcntl --enable-sockets --with-xmlrpc --enable-zip --enable-soap --with-gettext --enable-opcache --with-xsl
	
	make ZEND_EXTRA_LIBS='-liconv'
    make install
	

	echo "Copy new php configure file..."
    cp php.ini-development ${LnmpInstallDir%*/}/php72/etc/php.ini
	
	cd ${cur_dir}
    # php extensions
    echo "Modify php.ini......"
    sed -i 's/post_max_size =.*/post_max_size = 32M/g' ${LnmpInstallDir%*/}/php72/etc/php.ini
    sed -i 's/upload_max_filesize =.*/upload_max_filesize = 32/g' ${LnmpInstallDir%*/}/php72/etc/php.ini
    sed -i 's/;date.timezone =.*/date.timezone = Asia\/Shanghai/g' ${LnmpInstallDir%*/}/php72/etc/php.ini
    sed -i 's/short_open_tag =.*/short_open_tag = On/g' ${LnmpInstallDir%*/}/php72/etc/php.ini
    sed -i 's/;cgi.fix_pathinfo=.*/cgi.fix_pathinfo=0/g' ${LnmpInstallDir%*/}/php72/etc/php.ini
    sed -i 's/max_execution_time =.*/max_execution_time = 300/g' ${LnmpInstallDir%*/}/php72/etc/php.ini
	
	echo "Creating new php-fpm configure file..."
    cat > ${LnmpInstallDir%*/}/php72/etc/php-fpm.conf<<EOF
[global]
pid = run/php-fpm.pid
error_log = log/php-fpm.log
log_level = notice
include=${LnmpInstallDir%*/}/php72/etc/php-fpm.d/*.conf	
EOF

cat > ${LnmpInstallDir%*/}/php72/etc/php-fpm.d/www.conf<<EOF
[www]
listen = /tmp/php-fpm72.sock
listen.backlog = -1
listen.allowed_clients = 127.0.0.1
listen.owner = www
listen.group = www
listen.mode = 0666
user = www
group = www
pm = dynamic
pm.max_children = 10
pm.start_servers = 2
pm.min_spare_servers = 1
pm.max_spare_servers = 6
request_terminate_timeout = 100
request_slowlog_timeout = 0
slowlog = log/slow.log
EOF
	
	echo "Copy php-fpm init.d file..."
    cp ${cur_dir}/src/${Php_Ver72}/sapi/fpm/init.d.php-fpm /etc/init.d/php-fpm72
    chmod +x /etc/init.d/php-fpm72
}





Creat_PHP_Tools()
{
    echo "Create PHP Info Tool..."
	mkdir -p /home/wwwroot/default
    cat >/home/wwwroot/default/phpinfo.php<<eof
<?
phpinfo();
?>
eof

    

    cp ${cur_dir}/etc/index.html /home/wwwroot/default/index.html

}
