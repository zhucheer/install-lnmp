#!/bin/bash

Install_Tengine()
{
    Echo_Blue "[+] Installing ${Tengine_Ver}... "
    groupadd www
    useradd -s /sbin/nologin -g www www
	
	Tar_Cd ${Tengine_Ver}.tar.gz ${Tengine_Ver}
    ./configure --user=www --group=www --prefix=${LnmpInstallDir%*/}/tengine 
    make && make install
	
	if [ "$?" -eq 0 ];then
		Echo_Green "Tengine 安装成功"
		cd ${cur_dir}
		cp ./etc/tengine.conf ${LnmpInstallDir%*/}/tengine/conf/nginx.conf
	fi
	
	echo "Copy tengine init.d file..."
    cp ${cur_dir}/etc/init.d.tengine /etc/init.d/tengine
    chmod +x /etc/init.d/tengine
	
	echo "Modify php.ini......"
    sed -i "s:/usr/local/nginx:${LnmpInstallDir%*/}/tengine:g" /etc/init.d/tengine

}